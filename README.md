Kotlin cheatsheet
==================


* Cheats, tips and tricks for the Kotlin language
* Common design patterns in Kotlin

Usage: `$ KotlinCheatSheet/gradlew build`
Results in KotlinCheatSheet/build/reports/buildDashboard/index.html
