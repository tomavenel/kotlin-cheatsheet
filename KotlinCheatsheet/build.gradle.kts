import org.gradle.api.plugins.quality.*
import org.gradle.api.reporting.dependencies.HtmlDependencyReportTask
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.javadoc.Groovydoc
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.api.tasks.wrapper.Wrapper
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.jetbrains.dokka.gradle.DokkaTask
import org.gradle.api.plugins.ExtensionAware

import org.junit.platform.gradle.plugin.FiltersExtension
import org.junit.platform.gradle.plugin.EnginesExtension
import org.junit.platform.gradle.plugin.JUnitPlatformExtension

buildscript {

    repositories {
        jcenter()
        gradleScriptKotlin()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin"))
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:0.9.13") // Kotlin doc
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.0.0-M4")
    }
}

plugins {
    id("com.gradle.build-scan") version "1.8"
    `build-dashboard`
    `project-report`
    `build-announcements` // sends build notifications to desktop
    kotlin("jvm")
    jacoco
    idea
}

apply {
    plugin("org.jetbrains.dokka")
    plugin("org.junit.platform.gradle.plugin")
}

repositories {
    jcenter()
}

dependencies {
    //compile("com.github.kittinunf.result:result:1.1.0") //Result kotlin
    testCompile("org.jetbrains.spek:spek-api:1.1.2")
    testRuntime("org.jetbrains.spek:spek-junit-platform-engine:1.1.2")
    testCompile("org.jetbrains.kotlin:kotlin-test")
}

tasks {
    "dokkaJavadoc"(type = org.jetbrains.dokka.gradle.DokkaTask::class) {
        outputFormat = "javadoc"
        outputDirectory = "$buildDir/javadoc"
    }

    "dokkaMarkdown"(type = org.jetbrains.dokka.gradle.DokkaTask::class) {
        outputFormat = "markdown"
        outputDirectory = "$buildDir/dokkaMarkdown"
    }

    "doc" {
        dependsOn(
                tasks.withType(Javadoc::class.java),
                tasks.withType(Groovydoc::class.java),
                tasks.withType(DokkaTask::class.java),
                tasks.withType(JacocoReport::class.java)
        )
    }

    "wrapper"(type = Wrapper::class) {
        gradleVersion = "4.0.1"
    }
}


// extension for configuration
fun JUnitPlatformExtension.filters(setup: FiltersExtension.() -> Unit) {
    when (this) {
        is ExtensionAware -> extensions.getByType(FiltersExtension::class.java).setup()
        else -> throw Exception("${this::class} must be an instance of ExtensionAware")
    }
}
fun FiltersExtension.engines(setup: EnginesExtension.() -> Unit) {
    when (this) {
        is ExtensionAware -> extensions.getByType(EnginesExtension::class.java).setup()
        else -> throw Exception("${this::class} must be an instance of ExtensionAware")
    }
}


/*
buildScan {
    setLicenseAgreementUrl("https://gradle.com/terms-of-service")
    setLicenseAgree("yes")
    publishAlways()
}
*/
