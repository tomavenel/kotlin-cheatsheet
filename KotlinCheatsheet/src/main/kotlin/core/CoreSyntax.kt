package core

/**
 * The core Kotlin syntax
 */
class CoreSyntax {
}

class SampleCalculator {
    fun sum(x: Int, y: Int) = x + y
    fun substract(x: Int, y: Int) = x - y
}
